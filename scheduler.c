#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <pthread.h>

#define _MAX_EVENTS 10
#define _MAX_DESCRIPTION 101

typedef struct {

    int hour;
    int minute;
    char description[_MAX_DESCRIPTION];

} event;
int main ()
{

        pthread_t hilo1;
        pthread_create(&hilo1,NULL, ejecutarLista(list,i),NULL);
        pthread_join(hilo1,NULL);
}

    void *ejecutarLista(list,i)(void *args){
    int i, x=1,y=2;
    printf("Esperando para bloquear llave \n\n");
    pthread_mutex_lock(&llave);
    printf("Llave bloqueada \n\n");
    printf("Ejecutando procesos \n\n");
    for(int i=0; i<50; i++){
        delay(4);
    }
    for (i=0; i>elementos; i++){
        fflush(stdout);
        printf("\n proceso en ejecucion --- %s", s1[i].description);
        printf("\n tiempo --- %d", s1[i].hour);
        printf("\n tiempo --- %d", s1[i].minute);
        do {
            printf("\n[%d] seg", s1[i].time_execute);
            s1[i].time_execute=s1[i].time_execute -1;
            for(int 1=0; i<50;i++){
                delay(3);
            }
        }while (s1[i].time_execute !=0);
    }
    }system("clear");

void printMenu() {

    puts("+--- SCHEDULER ---+\n"
        "  1. Nuevo         \n"
        "  2. Eliminar      \n"
        "  3. Mostrar       \n"
        "  4. Finalizar       \n"
        "+------------------+\n");

}

bool isNull(const event *e) { return e == NULL; }

event *initEvent() {
    event *e = (event*)malloc(sizeof(event));

    e->hour = 0;
    e->minute = 0;
    strcpy(e->description, "");

    return e;
}

int inputRange(const int min, const int max) {

    int input = 0;
    char temp[21];
    char *prompt = "| Tiempo %d and  %d: ";

    printf(prompt, min, max);

    fgets(temp, 21, stdin);
    input = atoi(temp);

    while (input > max || input < min) {
        printf(prompt, min, max);
        fgets(temp, 21, stdin);
        input = atoi(temp);
    }

    return input;

}

event* newEvent(event *e) {

    if (isNull(e)) {
        e = initEvent();
    }

    char *seperator = "+--------------------------------+";

    printf("\n%s\n|           NUEVO EVENTO            |\n%s\n\n", seperator, seperator);

    puts("+---------- TIEMPO DE EVENTO----------+");

    e->hour = inputRange(0, 23);
    e->minute = inputRange(0, 59);

    puts(seperator);

    puts("\n+--- DESCRIPCION ---+");

    printf("%s", "| Descripcion: ");

    fgets(e->description, _MAX_DESCRIPTION, stdin);

    puts("+-------------------------+\n");

    puts("| Evento fue agregado.\n");

    return e;

}


void addEventAtIndex(event list[], const event e, const int i) {

    if (isNull(&e)) {
        return;
    }

    list[i].hour = e.hour;
    list[i].minute = e.minute;
    strcpy(list[i].description, e.description);

}


void sort(event list[], const int size) {

    for (int i = 1; i < size; i++) {
        for (int j = i; j > 0 && (list[j - 1].hour > list[j].hour || (list[j - 1].hour == list[j].hour && list[j - 1].minute > list[j].minute)); j--) {
            int hourJ = list[j].hour;
            int minuteJ = list[j].minute;
            char descriptionJ[_MAX_DESCRIPTION];
            strcpy(descriptionJ, list[j].description);

            int hourJMinus1 = list[j - 1].hour;
            int minuteJMinus1 = list[j - 1].minute;
            char descriptionJMinus1[_MAX_DESCRIPTION];
            strcpy(descriptionJMinus1, list[j - 1].description);

            list[j].hour = hourJMinus1;
            list[j].minute = minuteJMinus1;
            strcpy(list[j].description, descriptionJMinus1);

            list[j - 1].hour = hourJ;
            list[j - 1].minute = minuteJ;
            strcpy(list[j - 1].description, descriptionJ);
        }
    }

}


void sortInsert(event list[], int *size, event e) {

    addEventAtIndex(list, e, *size);

    (*size)++;
    sort(list, *size);

}

void printEvent(const event e) {

    char h1 = { (e.hour / 10) + '0' };
    char h2 = { (e.hour - (e.hour / 10) * 10) + '0' };

    char m1 = { (e.minute / 10) + '0' };
    char m2 = { (e.minute - (e.minute / 10) * 10) + '0' };

    printf("%c%c:%c%c - %s", h1, h2, m1, m2, e.description);

}


void printEventList(const event list[], const int size) {

    if (size == 0) {
        puts("\n| No existe evento\n");
        return;
    }

    char *seperator = "+--------------------------------+";

    printf("\n%s\n|   SCHEDULER           |\n%s\n\n", seperator, seperator);

    for (int i = 0; i < size; i++) {
        printf("| [%d] ", i);
        printEvent(list[i]);

    }

    putchar('\n');

}

void deleteEvent(event list[], int *size) {

    if (*size == 0) {
        puts("\n| Lista de evento vacia.\n");
        return;
    }

    char temp[21];
    int id;

    char *seperator = "\n+--------------------------------+";
    printf("%s\n|          ELIMINAR         |%s\n\n", seperator, seperator);

    for (int i = 0; i < *size; i++) {
        printf("| [%d] ", i);
        printEvent(list[i]);
    }

    printf("%s", "\n| Ingresa un ID para eliminar el evento:  ");

    fgets(temp, 21, stdin);
    id = atoi(temp);

    if (id > *size - 1) {
        printf("\n| No se localizo el evento %d\n", id);
        return;
    }

    printf("| Evento [%d] eliminado exitosamente.\n\n", id);

    list[id].hour = 99;
    list[id].minute = 99;
    strcpy(list[id].description, "");

    if (id != (*size - 1)) {
        sort(list, *size);
    }

    (*size)--;

}

char *encode(char *s) {

    for (int i = 0; i < strlen(s); i++) {
        if (s[i] == ' ') {
            s[i] = '_';
        }
    }

    return s;

}

char *decode(char *s) {

    for (int i = 0; i < strlen(s); i++) {
        if (s[i] == '_') {
            s[i] = ' ';
        }
    }

    return s;

}
void saveEventList(char *filename, event list[], int size) {

    FILE *f = fopen(filename, "w");

    if (f == NULL) {
        return;
    }

    for (int i = 0; i < size; i++) {
        fprintf(f, "%d %d %s", list[i].hour, list[i].minute, encode(list[i].description));
    }

    printf("\n| %d %s exitosamente se guardo en \"%s\".\n\n", size, (size == 1) ? "event" : "events", filename);

    fclose(f);

}

void loadEventList(char *filename, event list[], int *size) {

    FILE *f = fopen(filename, "r");
    char temp[6 + _MAX_DESCRIPTION];

    if (f == NULL) {
        printf("\n| File \"%s\" guardado.\n\n", filename);
        return;
    }

    *size = 0;

    while (fgets(temp, sizeof(temp), f)) {

        char *word = strtok(temp, " ");
        list[*size].hour = atoi(word);

        word = strtok(NULL, " ");
        list[*size].minute = atoi(word);

        word = strtok(NULL, " ");
        strcpy(list[*size].description, decode(word));
        (*size)++;

    }

    printf("\n| %d %s successfully loaded from \"%s\".\n", *size, (*size == 1) ? "event" : "events", filename);

    printEventList(list, *size);

}

int main() {

    event list[_MAX_EVENTS];
    int index = 0;
    int selection = 0;
    char file[FILENAME_MAX];
    char response = 'Y';
    char temp[21];

    while (selection != 6) {

        printMenu();

        printf("%s", "|  selecciona una opcion: ");
        fgets(temp, 21, stdin);
        selection = atoi(temp);

        switch (selection) {

        case 1:
            if (index + 1 > _MAX_EVENTS) {
                printf("| Solo puede tener %d eventos activos a la vez!\n\n", index);
                break;
            }
            sortInsert(list, &index, *newEvent(&list[index]));
            break;
        case 2: // Delete Event
            deleteEvent(list, &index);
            break;
        case 3: // Display Schedule
            printEventList(list, index);
            break;

        case 4:
            puts("\n| Termino!\n");
            break;
        default:
            puts("\n| Error en seleccion\n");
            break;

        }

    }

}

